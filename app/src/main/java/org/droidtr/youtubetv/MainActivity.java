package org.droidtr.youtubetv;

import android.app.*;
import android.os.*;
import android.view.*;
import android.webkit.*;
import android.widget.*;
import android.view.View.*;

public class MainActivity extends Activity 
{
	WebView w; 
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		
		w= new WebView(this);
        super.onCreate(savedInstanceState);
		LinearLayout ll = new LinearLayout(this);
		setContentView(ll);
		w.getSettings().setJavaScriptEnabled(true);
		w.getSettings().setUserAgentString("Mozilla/5.0 (SMART-TV;Linux;Tizen 2.3) AppleWebkit/538.1 (KHTML,like Gecko) SamsungBrowser/1.0 Safari/538.1");
		w.setLayoutParams(ll.getLayoutParams());
		w.getSettings().setAppCacheEnabled(true);
		w.getSettings().setSaveFormData(true);
		w.getSettings().setDatabaseEnabled(true);
		w.getSettings().setDomStorageEnabled(true);
		w.setWebChromeClient(new WebChromeClient());
		w.setWebViewClient(new WebViewClient(){
			public boolean shouldOverrideUrlLoading(WebView w,String url){
				return super.shouldOverrideUrlLoading(w,url);	
			}
		});
		w.setFocusable(false);
		w.getSettings().setLoadWithOverviewMode(true);
		w.getSettings().setPluginState(WebSettings.PluginState.ON);
		w.getSettings().setAllowContentAccess(true);
		w.getSettings().setAllowFileAccess(true);
		w.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		w.loadUrl("https://youtube.com/tv");
		ll.addView(w);
    }

	@Override
	public void onBackPressed()
	{
		if(w.canGoBack()){
		w.goBack();
		}else{
			w.reload();
		}
	}
	
}
